# Ansible Role Linux Pumped ZSH



## Description

This role performs the following operations

- Installs ZSH (if not already installed)
- Installs oh-my-zsh
- Installs zsh-autosuggestions
- Configures ZSH for the SSH user that connected through Ansible
- Uses the Ansible Role Linux Nerd Fonts to install nerd fonts if not yet present
